import os

import geopandas as gpd

from sbk.analysis.descriptors import Shadow, DNI, Batches, Bakable
from sbk.bakery.oven import SolarOven
from sbk.bakery.models import SolarThresholdModel
from sbk.solar_db.sarah2 import Sarah2

from pyrasta.raster import DigitalElevationModel

out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/RESULTS/HAUTE_SAVOIE"
padded_dem_file = "/media/benjamin/STORAGE/BOUSOLRUN/DEM/haute_savoie_padded.tif"
bounded_dem_file = "/media/benjamin/STORAGE/BOUSOLRUN/DEM/haute_savoie_bounded.tif"
df = gpd.GeoDataFrame.from_file("/media/benjamin/STORAGE/BOUSOLRUN/DEPARTEMENTS/haute_savoie.geojson")
df = df.to_crs(epsg=4326)
directory = "/home/benjamin/Documents/PRO/CMSAF/2018"
h5_from = "/media/benjamin/STORAGE/BOUSOLRUN/H5/haute_savoie"
h5_to = os.path.join(out_dir, "descriptors")
oven = SolarOven(SolarThresholdModel(700, 2, 1))
solar_db = Sarah2(directory, h5_from)
solar_db = solar_db.clip(region=df)
solar_db._padded_dem = DigitalElevationModel(padded_dem_file)
solar_db._bounded_dem = DigitalElevationModel(bounded_dem_file)

DNI(oven, solar_db, h5_to).run(nb_processes=24, chunk_size=10000)
dni = DNI(oven, solar_db, h5_to).compute_raster(period="D", nb_processes=24, chunk_size=10000)
dni.to_file(os.path.join(out_dir, "daily_dni.tif"))

batches = Batches(oven, solar_db, h5_to)
batches.run(nb_processes=24, chunk_size=10000)
nb_batches = batches.compute_raster(period=None, nb_processes=24, chunk_size=10000)
nb_batches.to_file(os.path.join(out_dir, "nb_batches.tif"))

Bakable(batches, h5_to).run(nb_processes=24, chunk_size=10000)
bakable = Bakable(batches, h5_to).compute_raster(period=None, nb_processes=24, chunk_size=10000)
bakable.to_file(os.path.join(out_dir, "bakable_days.tif"))
