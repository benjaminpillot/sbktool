from rtree import index


def get_slice_along_axis(ndim, axis, _slice):
    """ Used to make indexing with any n-dimensional numpy array

    Parameters
    ----------
    ndim: int
        number of dimensions
    axis: int
        axis for which we want the slice
    _slice: slice
        the required slice

    Returns
    -------
    """
    slc = [slice(None)] * ndim
    slc[axis] = _slice

    return tuple(slc)


def intersecting_features(geometry, geometry_collection, r_tree):
    """ Return list of geometries intersecting with given geometry

    Parameters
    ----------
    geometry
    geometry_collection
    r_tree

    Returns
    -------

    """
    is_intersecting = intersects(geometry, geometry_collection, r_tree)
    return [i for i in range(len(geometry_collection)) if is_intersecting[i]], \
           [geom for i, geom in enumerate(geometry_collection) if is_intersecting[i]]


def intersects(geometry, geometry_collection, r_tree):
    """ Return if geometry intersects with geometries in collection

    Parameters
    ----------
    geometry
    geometry_collection
    r_tree

    Returns
    -------

    """
    if r_tree is None:
        r_tree = r_tree_idx(geometry_collection)

    list_of_intersecting_features = list(r_tree.intersection(geometry.bounds))

    return [False if f not in list_of_intersecting_features
            else geometry.intersects(geometry_collection[f]) for f in
            range(len(geometry_collection))]


def is_iterable(iterable):
    """

    Parameters
    ----------
    iterable

    Returns
    -------

    """
    try:
        iter(iterable)
        return True
    except TypeError:
        return False


def lazyproperty(func):
    name = '_lazy_' + func.__name__

    @property
    def lazy(self):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            value = func(self)
            setattr(self, name, value)
            return value
    return lazy


def r_tree_idx(geometry_collection):
    """ Return Rtree spatial index of geometry collection

    Parameters
    ----------
    geometry_collection

    Returns
    -------

    """
    idx = index.Index()
    if not is_iterable(geometry_collection):
        geometry_collection = [geometry_collection]

    for i, geom in enumerate(geometry_collection):
        idx.insert(i, geom.bounds)

    return idx
