import os

import geopandas as gpd

from sbk.solar_db.sarah2 import Sarah2


directory = "/home/benjamin/Documents/PRO/CMSAF/2018"
departements = "/media/benjamin/STORAGE/BOUSOLRUN/DEPARTEMENTS"

out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/H5"
out_dir_dem = "/media/benjamin/STORAGE/BOUSOLRUN/DEM"

dep_name = ("herault", "haute_savoie", "pyrenees_atlantiques",
            "seine_maritime", "bouches_du_rhone", "puy_de_dome",
            "cotes_d_armor")


for rname in dep_name:

    if not os.path.exists(os.path.join(out_dir, rname)):

        df = gpd.GeoDataFrame.from_file(os.path.join(departements, rname + ".geojson"))
        h5file = os.path.join(out_dir, rname)
        region = df.to_crs(epsg=4326)

        # Computation
        solar_db = Sarah2(directory, h5file)
        new_solar_db = solar_db.clip(region=region)._fetch_dem()

        new_solar_db._bounded_dem.to_file(os.path.join(out_dir_dem, rname + "_bounded.tif"))
        new_solar_db._padded_dem.to_file(os.path.join(out_dir_dem, rname + "_padded.tif"))

        new_solar_db._compute_horizons(distance=0.2, precision=1, nb_processes=24, chunk_size=10000)
        new_solar_db._compute_horizon_vs_sun(nb_processes=24, chunk_size=1000)
        new_solar_db.compute_dni()
