import os

from pyrasta.raster import DigitalElevationModel

from sbk.bakery.models import SolarThresholdModel
from sbk.bakery.oven import SolarOven
from sbk.analysis.descriptors import Shadow
from sbk.shading.solar_position import compute_horizon_vs_sun
from sbk.solar_db.sarah2 import Sarah2

bounded_dem_file = "/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA/TESTS/herault_bounded_dem.tif"
padded_dem_file = "/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA/TESTS/herault_padded_dem.tif"

out_dir = "/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA/TESTS"
h5path = os.path.join(out_dir, "test2")
h5_to = os.path.join(out_dir, "shadow")

bounded_dem = DigitalElevationModel(bounded_dem_file)
padded_dem = DigitalElevationModel(padded_dem_file)

# compute_horizons(bounded_dem, padded_dem_file, h5path,
#                  "horizon", distance=0.2, precision=1,
#                  nb_processes=24, chunk_size=1000)

# time = pd.date_range("01-01-2018 0:00", "31-12-2018 23:00", freq="30min")
directory = "/home/benjamin/Documents/PRO/CMSAF/2018_2020"
solar_db = Sarah2(directory, h5path)
solar_db._bounded_dem = bounded_dem
solar_db._padded_dem = padded_dem

compute_horizon_vs_sun(bounded_dem, solar_db.time, h5path, "horizon_vs_sun", "horizon", nb_processes=24,
                       chunk_size=1000)

oven = SolarOven(SolarThresholdModel(700, 2, 1))
Shadow(oven, solar_db, h5_to).run(nb_processes=24, chunk_size=1000)
shadow = Shadow(oven, solar_db, h5_to).compute_raster(period="Y", nb_processes=24, chunk_size=1000)
shadow.to_file(os.path.join(out_dir, "shadow.tif"))



