import multiprocessing as mp
from functools import partial

import numpy as np

from pyproj import CRS
from pyrasta.raster import DigitalElevationModel
from pyrasta.utils import grid, split_into_chunks
from tqdm import tqdm

from sbk.iotools import H5File


def get_horizon(location, padded_dem, distance, precision):
    """ Get horizon from dem on specific location

    Parameters
    ----------
    location: tuple or list
        tuple of coordinates (lat, lon, altitude)
    padded_dem: str
        valid path to padded DEM file
    distance
    precision

    Returns
    -------

    """
    dem = DigitalElevationModel(padded_dem)
    if location[2] != dem.no_data:
        return horizon(location[0], location[1],
                       dem=dem,
                       distance=distance,
                       precision=precision)["elevation"]
    else:
        return np.zeros((360 + precision)//precision)


def compute_horizons(padded_dem, longitude, latitude, altitude,
                     h5_path, h5_ds_name, distance, precision,
                     nb_processes, chunk_size):
    """ Compute horizons over a DEM tile

    Parameters
    ----------
    padded_dem: str
        Valid path to padded dem file
    longitude
    latitude
    altitude
    h5_path: str
        path to H5 file
    h5_ds_name: str
        H5 dataset name
    distance: float
    precision: int or float
    nb_processes: int
    chunk_size: int

    Returns
    -------

    """
    # longitude, latitude = np.meshgrid(bounded_dem.grid_x, bounded_dem.grid_y)
    location_generator = ((lat, lon, z) for (lat, lon, z) in zip(latitude,
                                                                 longitude,
                                                                 altitude))

    # Create h5 dataset
    with H5File(h5_path) as h5:
        h5.reset_dataset(h5_ds_name,
                         shape=(altitude.size,
                                (360 + precision) // precision),
                         dtype=np.float32)

        pg = tqdm(total=altitude.size // chunk_size + 1,
                  desc="Compute horizons")
        for loc_generator in split_into_chunks(location_generator, chunk_size):

            with mp.Pool(processes=nb_processes) as pool:
                horz = np.asarray(list(pool.map(partial(get_horizon,
                                                        padded_dem=padded_dem,
                                                        distance=distance,
                                                        precision=precision),
                                                loc_generator,
                                                chunksize=None)))

            h5.append(h5_ds_name, horz)
            pg.update(1)
        pg.close()

    return 0


def horizon(latitude, longitude, dem, distance,
            precision, ellipsoid=CRS(4326).get_geod()):
    """ Retrieve terrain horizon on given location

    Parameters
    ----------
    latitude
    longitude
    dem: pyraster.raster.DigitalElevationModel
    distance
    precision
    ellipsoid

    Returns
    -------

    """
    # Prune DEM and fit to study area
    bounds = (max(longitude - distance - dem.resolution[0] / 2, dem.bounds[0]),
              max(latitude - distance - dem.resolution[1] / 2, dem.bounds[1]),
              min(longitude + distance + dem.resolution[0] / 2, dem.bounds[2]),
              min(latitude + distance + dem.resolution[1] / 2, dem.bounds[3]))

    study_array = dem.read_array(bounds=bounds)

    # Lat/lon meshgrid
    grid_x = np.asarray([lon for lon in grid(bounds[0] + dem.resolution[0] / 2,
                                             dem.resolution[0], study_array.shape[1])])
    grid_y = np.asarray([lat for lat in grid(bounds[3] - dem.resolution[1] / 2,
                                             -dem.resolution[1], study_array.shape[0])])

    x_obs = np.argmin(np.abs(longitude - grid_x))
    y_obs = np.argmin(np.abs(latitude - grid_y))
    z_obs = study_array[y_obs, x_obs]
    lon_grid, lat_grid = np.meshgrid(grid_x, grid_y)

    # Azimuth and elevation
    azimuth = (180 / np.pi) * get_azimuth(latitude * np.pi / 180, longitude * np.pi / 180,
                                          (lat_grid - dem.resolution[1] / 2) * np.pi / 180,
                                          (lon_grid + dem.resolution[0] / 2) * np.pi / 180,
                                          ellipsoid.es)
    elevation = np.zeros(azimuth.shape)
    elevation[study_array > z_obs] = \
        get_elevation(z_obs, study_array[study_array > z_obs], latitude * np.pi / 180,
                      lat_grid[study_array > z_obs] * np.pi / 180,
                      longitude * np.pi / 180, lon_grid[study_array > z_obs]
                      * np.pi / 180, ellipsoid.es, ellipsoid.a)
    # TODO: understand why "z_obs < study_area" return a numpy ValueError (ambiguous truth value)

    # Elevation vector length
    len_elevation = (90 + precision) // precision

    elevation_dic = dict(ne=np.zeros((y_obs, len_elevation)),
                         e=np.zeros((study_array.shape[1] - x_obs, 2 * len_elevation - 1)),
                         s=np.zeros((study_array.shape[0] - y_obs, 2 * len_elevation - 1)),
                         w=np.zeros((x_obs, 2 * len_elevation - 1)),
                         nw=np.zeros((y_obs, len_elevation)))

    azimuth_dic = dict(ne=np.arange(-180, -90 + precision, precision),
                       e=np.arange(-180, 0 + precision, precision),
                       s=np.arange(-90, 90 + precision, precision),
                       w=np.arange(0, 180 + precision, precision),
                       nw=np.arange(90, 180 + precision, precision))

    # Main computation
    # NE & NW
    for n, (az, el) in enumerate(zip(azimuth[:y_obs], elevation[:y_obs])):
        idx_ne = np.digitize(azimuth_dic["ne"], az[x_obs:])
        idx_nw = np.digitize(azimuth_dic['nw'], az[:x_obs])
        elevation_dic["ne"][n, idx_ne < len(az[x_obs:])] = el[x_obs:][idx_ne[idx_ne <
                                                                             len(az[x_obs:])]]
        elevation_dic["nw"][n, idx_nw < len(az[:x_obs])] = el[:x_obs][idx_nw[idx_nw <
                                                                             len(az[:x_obs])]]

    # South
    for n, (az, el) in enumerate(zip(azimuth[y_obs:, ::-1], elevation[y_obs:, ::-1])):
        idx_s = np.digitize(azimuth_dic["s"], az)
        elevation_dic["s"][n, idx_s < len(az)] = el[idx_s[idx_s < len(az)]]

    # East
    for n, (az, el) in enumerate(zip(azimuth[:, x_obs:].transpose(),
                                     elevation[:, x_obs:].transpose())):
        idx_e = np.digitize(azimuth_dic["e"], az)
        elevation_dic["e"][n, idx_e < len(az)] = el[idx_e[idx_e < len(az)]]

    # West
    for n, (az, el) in enumerate(zip(azimuth[::-1, :x_obs].transpose(),
                                     elevation[::-1, :x_obs].transpose())):
        idx_w = np.digitize(azimuth_dic["w"], az)
        elevation_dic["w"][n, idx_w < len(az)] = el[idx_w[idx_w < len(az)]]

    sun_mask = np.concatenate([elevation_dic[key].max(axis=0) for key in elevation_dic.keys()])
    az_mask = np.concatenate([azimuth_dic[key] for key in azimuth_dic.keys()]) + 180

    skyline = dict(elevation=np.zeros((360 + precision) // precision),
                   azimuth=np.arange(0, 360 + precision, precision))
    for n, az in enumerate(skyline["azimuth"]):
        skyline["elevation"][n] = np.max(sun_mask[az_mask == az])

    skyline["elevation"][-1] = skyline["elevation"][0]

    return skyline


def geo_to_cartesian(latitude, longitude, altitude, e, a):
    """

    Parameters
    ----------
    latitude
    longitude
    altitude
    e
    a

    Returns
    -------

    """
    n = a / np.sqrt(1 - (e**2) * (np.sin(latitude))**2)

    # return X, Y, Z
    return (n + altitude) * np.cos(longitude) * np.cos(latitude), \
           (n + altitude) * np.sin(longitude) * np.cos(latitude),\
           (n * (1 - e**2) + altitude) * np.sin(latitude)


def get_azimuth(lat1, lon1, lat2, lon2, e):
    """

    Parameters
    ----------
    lat1
    lon1
    lat2
    lon2
    e

    Returns
    -------

    """

    # Retrieve isometric latitude
    iso_lat1 = get_isometric_latitude(lat1, e)
    iso_lat2 = get_isometric_latitude(lat2, e)

    # Compute azimuth
    return np.arctan2((lon1 - lon2), (iso_lat1 - iso_lat2))


def get_elevation(h_a, h_b, latitude_a, latitude_b, longitude_a, longitude_b, e, a):
    """

    Parameters
    ----------
    h_a
    h_b
    latitude_a
    latitude_b
    longitude_a
    longitude_b
    e
    a

    Returns
    -------

    """
    x_a, y_a, z_a = geo_to_cartesian(latitude_a, longitude_a, h_a, e, a)
    x_b, y_b, z_b = geo_to_cartesian(latitude_b, longitude_b, h_b, e, a)

    inner_prod = (x_b - x_a) * np.cos(longitude_a) * np.cos(latitude_a) + (y_b - y_a) \
        * np.sin(longitude_a) * np.cos(latitude_a) + (z_b - z_a) * np.sin(latitude_a)

    # Cartesian norm
    norm = np.sqrt((x_b - x_a) ** 2 + (y_b - y_a) ** 2 + (z_b - z_a) ** 2)

    return np.arcsin(inner_prod/norm) * 180/np.pi


def get_isometric_latitude(latitude, e):
    """

    Parameters
    ----------
    latitude
    e

    Returns
    -------

    """
    term_1 = np.tan((np.pi/4) + (latitude/2))
    term_2 = ((1 - e * np.sin(latitude)) / (1 + e * np.sin(latitude))) ** (e / 2)

    return np.log(term_1 * term_2)
