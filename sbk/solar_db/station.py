from sbk.solar_db.base import SparseSolarDB
from sbk.utils import lazyproperty


class Station(SparseSolarDB):

    def __init__(self, directory, h5_path):
        """

        Parameters
        ----------
        directory
        h5_path
        """
        super().__init__(h5_path)

        # Implement here the function to get station time series

    @property
    def time_resolution(self):
        pass

    def get_dni(self):
        pass

    def get_ghi(self):
        pass

    @property
    def latitude(self):
        pass

    @property
    def longitude(self):
        pass

    @property
    def no_data(self):
        pass

    @lazyproperty
    def time(self):
        pass
