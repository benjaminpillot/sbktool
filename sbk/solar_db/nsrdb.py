from sbk.solar_db.base import SolarDB

AWS_TEMPLATE = "/nrel/nsrdb/v3/nsrdb_%d.h5"


class NSRDB(SolarDB):

    # def __init__(self, year):
    #
    #     super().__init__()
    #
    #     try:
    #         self.h5 = h5pyd.File(AWS_TEMPLATE % year, "r")
    #     except OSError:
    #         raise ValueError(f"Invalid value: {year}")
    #
    #     self._points = [Point(c[1], c[0]) for c in self.h5['coordinates'][...]]

    def _compute_dni(self, dni, *args, **kwargs):
        pass

    def _get_coord_for_horizon_computation(self):
        pass

    def get_dni(self):
        pass

    def get_ghi(self):
        pass

    @property
    def latitude(self):
        pass

    @property
    def longitude(self):
        pass

    @property
    def no_data(self):
        pass

    def time(self):
        pass

    @property
    def time_resolution(self):
        pass

    @property
    def x_size(self):
        pass

    @property
    def y_size(self):
        pass
