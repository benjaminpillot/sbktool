from copy import copy

import netCDF4 as nc
import numpy as np
import pandas as pd
from pyproj import CRS
from tqdm import tqdm

from sbk.solar_db import CM_SAF_SARAH2_ORIGIN_TIME
from sbk.iotools import find_file
from sbk.solar_db.base import RegularSolarDB
from sbk.utils import lazyproperty


class Sarah2(RegularSolarDB):

    _origin_date = pd.Timestamp(CM_SAF_SARAH2_ORIGIN_TIME)
    _time_dimension = 0

    crs = CRS(4326)

    def __init__(self, directory, h5_path):
        """

        Parameters
        ----------
        directory: str
            directory where looking for netcdf files
            must only contains netcdf files
            must be geographically coherent files (same boundaries)
        h5_path: str
            valid path to H5 file used to store instance data
        """
        super().__init__(h5_path)
        self._files = find_file(".nc", directory)
        self._dataset = [nc.Dataset(file, 'r') for file in self._files]
        self._base_latitude = self._dataset[0].variables["lat"][:]
        self._base_longitude = self._dataset[0].variables["lon"][:]
        self._bounds = (min(self._base_longitude),
                        min(self._base_latitude),
                        max(self._base_longitude),
                        max(self._base_latitude))
        self._bounds_idx = (0, 0, self._base_longitude.size, self._base_latitude.size)

    def get_dni(self):
        """ Return array of DNI values for the zone of interest

        Returns
        -------

        """
        minx, miny, maxx, maxy = self._bounds_idx

        return np.concatenate([ds.variables["DNI"][:, miny:maxy, minx:maxx]
                               for ds in tqdm(self._dataset, desc="Retrieve DNI")])

    def get_ghi(self):
        pass

    @property
    def no_data(self):
        return self._dataset[0].variables["DNI"].missing_value

    @lazyproperty
    def time(self):
        """ Return time timeseries of Sarah2 object

        Time is equal to "origin date + nb of hours"
        """
        hours = np.concatenate([ds.variables["time"][:] for ds in self._dataset])

        return pd.DatetimeIndex([self._origin_date + pd.Timedelta(hours=h) for h in hours])

    @property
    def time_resolution(self):
        return 24 / self._dataset[0].dimensions['time'].size
