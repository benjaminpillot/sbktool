import multiprocessing as mp
from copy import copy

import numpy as np

from abc import abstractmethod

from sbk.shading.dem import _fetch_dem
from sbk.shading.horizon import compute_horizons
from sbk.iotools import H5File
from sbk.shading.solar_position import compute_horizon_vs_sun
from sbk.shading.solar_radiation import _shaded_dni_grid, _shaded_dni_sparse, _dni_grid
from sbk import SOLAR_RADIATION_SCALE_FACTOR, H5_SHADED_DNI_DS_NAME, H5_HORIZON_DS_NAME, \
    H5_HORIZON_VS_SUN_DS_NAME, H5_SATELLITE_DNI_DS_NAME
from sbk.utils import lazyproperty


SPARSE_Y_SIZE = 1


class SolarDB:
    """ Solar database base class

    """

    _bounds = None
    _bounded_dem = None
    _padded_dem = None
    _solar_radiation_scale_factor = SOLAR_RADIATION_SCALE_FACTOR

    crs = None
    do_shading = False
    h5 = None
    dem_padding = None
    region = None

    h5_horizon_ds_name = H5_HORIZON_DS_NAME
    h5_horizon_vs_sun_ds_name = H5_HORIZON_VS_SUN_DS_NAME
    _h5_shaded_dni_ds_name = H5_SHADED_DNI_DS_NAME
    _h5_satellite_dni_ds_name = H5_SATELLITE_DNI_DS_NAME

    def __init__(self, h5_path):
        """ Initialize SolarDB instance

        Parameters
        ----------
        h5_path: str
            path to H5 file
        """
        try:
            h5 = H5File(h5_path)
        except FileNotFoundError:
            raise TypeError(f"'{h5_path}' is not a valid path name")
        else:
            self.h5 = h5_path
            h5.close()

    @abstractmethod
    def _compute_dni(self, dni, *args, **kwargs):
        pass

    def _compute_horizons(self, distance, precision,
                          nb_processes, chunk_size):
        """ Compute horizons over DEM points

        Parameters
        ----------
        distance
        precision
        nb_processes
        chunk_size

        Returns
        -------

        """
        if not self._padded_dem:
            raise ValueError("No DEM to compute horizons from. Fetch DEM beforehand")

        if distance > self.dem_padding:
            raise ValueError("Distance exceeds DEM boundaries")

        latitude, longitude, altitude = self._get_coord_for_horizon_computation()

        return compute_horizons(padded_dem=self._padded_dem._file,
                                longitude=longitude,
                                latitude=latitude,
                                altitude=altitude,
                                h5_path=self.h5,
                                h5_ds_name=self.h5_horizon_ds_name,
                                distance=distance,
                                precision=precision,
                                nb_processes=nb_processes,
                                chunk_size=chunk_size)

    def _compute_horizon_vs_sun(self, pressure, temperature,
                                delta_t, atmos_refrac,
                                nb_processes, chunk_size):
        """ Compute sun shading effect

        Parameters
        ----------
        pressure
        temperature
        delta_t
        atmos_refrac
        nb_processes
        chunk_size

        Returns
        -------

        """
        latitude, longitude, altitude = self._get_coord_for_horizon_computation()

        return compute_horizon_vs_sun(longitude=longitude,
                                      latitude=latitude,
                                      altitude=altitude,
                                      time=self.time,
                                      h5_path=self.h5,
                                      h5_ds_name=self.h5_horizon_vs_sun_ds_name,
                                      h5_horizon_ds_name=self.h5_horizon_ds_name,
                                      dem_no_data=self._bounded_dem.no_data,
                                      pressure=pressure,
                                      temperature=temperature,
                                      delta_t=delta_t,
                                      atmos_refrac=atmos_refrac,
                                      nb_processes=nb_processes,
                                      chunk_size=chunk_size)

    @abstractmethod
    def _compute_shaded_dni(self, dni, *args, **kwargs):
        pass

    def _fetch_dem(self, from_, padding):
        """ Fetch DEM corresponding to the area from online databases

        Parameters
        ----------
        from_: str
            online database DEM data must be retrieved from
            OR
            path to DEM file
        padding: int or float
            DEM padding around fetched DEM

        Returns
        -------

        """
        self._padded_dem, self._bounded_dem = _fetch_dem(from_,
                                                         region=self.region,
                                                         bounds=self._bounds,
                                                         dem_pad=padding)

        self.dem_padding = padding

        return self

    @abstractmethod
    def _get_coord_for_horizon_computation(self):
        """ Overload this method to get latitude, longitude
        and altitude for horizon-based computations

        Returns
        -------

        """
        pass

    @abstractmethod
    def clip(self, bounds=None, region=None, *args, **kwargs):
        pass

    def compute_dni(self, *args, **kwargs):
        """ Compute DNI

        Returns
        -------

        """
        # Retrieve DNI
        dni = self._solar_radiation_scale_factor * self.get_dni()

        # Compute DNI
        if self.do_shading:
            self._compute_shaded_dni(dni, *args, **kwargs)
        else:
            self._compute_dni(dni, *args, **kwargs)

    def compute_ghi(self):
        # TODO: implement GHI computation
        pass

    def compute_terrain(self, distance, precision=1,
                        from_="SRTM3", padding=0.5,
                        pressure=1013.25, temperature=12,
                        delta_t=67.0, atmos_refrac=0.5667,
                        nb_processes=mp.cpu_count(), chunk_size=100):
        """ Compute terrain effects for shaded DNI

        Parameters
        ----------
        distance: int or float
            horizon distance calculation
        precision: int or float
            horizon precision
        from_: str
            online database DEM data must be retrieved from
            OR
            path to DEM file
        padding: int or float
            padding around fetched DEM (for
            computing horizons near the boundaries)
            Default value in degrees (0.5)
        pressure
        temperature
        delta_t
        atmos_refrac
        nb_processes: int
            Nb of processes for multi processing
        chunk_size: int
            Chunk size for multiprocessing

        Returns
        -------

        """
        self._fetch_dem(from_, padding)
        self._compute_horizons(distance,
                               precision=precision,
                               nb_processes=nb_processes,
                               chunk_size=chunk_size)
        self._compute_horizon_vs_sun(pressure, temperature,
                                     delta_t, atmos_refrac,
                                     nb_processes=nb_processes,
                                     chunk_size=chunk_size)
        self.do_shading = True

        return self

    @classmethod
    def from_h5(cls, h5):
        """ Create instance from existing H5 file

        Parameters
        ----------
        h5: str
            path to existing H5 file

        Returns
        -------

        """
        pass

    @abstractmethod
    def get_dni(self):
        pass

    @abstractmethod
    def get_ghi(self):
        pass

    def save_h5(self):
        """ Save instance to H5 file

        Returns
        -------

        """
        with H5File(self.h5) as h5:
            for attr in ("do_shading", "bounds", "region", "dem_padding"):
                h5.attributes[attr] = self.__getattribute__(attr)

    @property
    def bounds(self):
        if self.do_shading:
            return self._bounded_dem.bounds
        else:
            return self._bounds

    @property
    def h5_dni_ds_name(self):
        if self.do_shading:
            return self._h5_shaded_dni_ds_name
        else:
            return self._h5_satellite_dni_ds_name

    @property
    @abstractmethod
    def latitude(self):
        pass

    @property
    @abstractmethod
    def longitude(self):
        pass

    @property
    @abstractmethod
    def no_data(self):
        pass

    @lazyproperty
    @abstractmethod
    def time(self):
        pass

    @property
    @abstractmethod
    def time_resolution(self):
        pass

    @property
    @abstractmethod
    def x_size(self):
        pass

    @property
    @abstractmethod
    def y_size(self):
        pass


class RegularSolarDB(SolarDB):
    """RegularSolarDB class definition
    a geo solar db is a regular grid solar database
    with cartographic outputs

    """
    _base_latitude = None
    _base_longitude = None
    _bounds_idx = None

    def _compute_dni(self, dni, *args, **kwargs):
        """

        Parameters
        ----------
        dni

        Returns
        -------

        """
        return _dni_grid(dni,
                         self._bounds,
                         self.time,
                         self.latitude,
                         self.longitude,
                         self.h5,
                         self.h5_dni_ds_name)

    def _compute_shaded_dni(self, dni, *args, **kwargs):

        return _shaded_dni_grid(dni,
                                self._bounded_dem,
                                self.time,
                                self.latitude,
                                self.longitude,
                                self.h5,
                                self.h5_dni_ds_name,
                                self.h5_horizon_vs_sun_ds_name)

    def _get_coord_for_horizon_computation(self):
        """

        Returns
        -------

        """
        longitude, latitude = np.meshgrid(self._bounded_dem.grid_x,
                                          self._bounded_dem.grid_y)
        altitude = self._bounded_dem.read_array()

        return latitude.ravel(), longitude.ravel(), altitude.ravel()

    def clip(self, bounds=None, region=None, padding=1, *args, **kwargs):
        """ Clip DB geo boundaries

        Parameters
        ----------
        bounds: list or tuple or numpy.ndarray
            geo bounds as (minx, miny, maxx, maxy)_base_longitude
        region: geopandas.GeoDataFrame
        padding: int

        Returns
        -------

        """
        if bounds is None:
            bounds = region.bounds.values.flatten()

        lat, lon = self._base_latitude, self._base_longitude

        # Set padding around geo boundaries while clipping
        minx = max(0, np.where(lon < bounds[0])[0][-1] - padding)
        maxx = min(np.where(lon > bounds[2])[0][0] + padding, lon.size)
        miny = max(0, np.where(lat < bounds[1])[0][-1] - padding)
        maxy = min(np.where(lat > bounds[3])[0][0] + padding, lat.size)

        # Compute new object
        new_solar_db = copy(self)
        new_solar_db._bounds_idx = minx, miny, maxx, maxy
        new_solar_db._bounds = lon[minx], lat[miny], lon[maxx], lat[maxy]
        new_solar_db.region = region

        return new_solar_db

    @abstractmethod
    def get_dni(self):
        pass

    @abstractmethod
    def get_ghi(self):
        pass

    @property
    def latitude(self):

        if self.do_shading:
            lat = self._bounded_dem.grid_y
        else:
            _, miny, _, maxy = self._bounds_idx
            lat = self._base_latitude[miny:maxy]

        return lat

    @property
    def longitude(self):

        if self.do_shading:
            lon = self._bounded_dem.grid_x
        else:
            minx, _, maxx, _ = self._bounds_idx
            lon = self._base_longitude[minx:maxx]

        return lon

    @property
    @abstractmethod
    def no_data(self):
        pass

    @lazyproperty
    @abstractmethod
    def time(self):
        pass

    @property
    @abstractmethod
    def time_resolution(self):
        pass

    @property
    def x_size(self):
        return self.longitude.size

    @property
    def y_size(self):
        return self.latitude.size


class SparseSolarDB(SolarDB):
    """SparseSolarDB class definition
    a sparse solar DB is a simple set of locations
    with punctual solar radiation outputs
    (ex.: weather stations/pyranometers, etc.)

    """

    def _compute_dni(self, dni, *args, **kwargs):
        """

        Parameters
        ----------
        dni

        Returns
        -------

        """
        pass

    def _compute_shaded_dni(self, dni, nb_processes=mp.cpu_count(), chunk_size=1000):
        return _shaded_dni_sparse(dni,
                                  h5_path=self.h5,
                                  h5_shaded_dni_ds_name=self.h5_dni_ds_name,
                                  h5_horizon_vs_sun_ds_name=self.h5_horizon_vs_sun_ds_name,
                                  nb_processes=nb_processes,
                                  chunk_size=chunk_size)

    def _get_coord_for_horizon_computation(self):

        altitude = np.asarray([self._bounded_dem.read_value_at(lon, lat)
                               for lon, lat in zip(self.longitude, self.latitude)])

        return self.latitude, self.longitude, altitude

    def clip(self, bounds=None, region=None, *args, **kwargs):
        pass

    @abstractmethod
    def get_dni(self):
        pass

    @abstractmethod
    def get_ghi(self):
        pass

    @property
    @abstractmethod
    def latitude(self):
        pass

    @property
    @abstractmethod
    def longitude(self):
        pass

    @property
    @abstractmethod
    def no_data(self):
        pass

    @lazyproperty
    @abstractmethod
    def time(self):
        pass

    @property
    @abstractmethod
    def time_resolution(self):
        pass

    @property
    def x_size(self):
        return self.longitude.size

    @property
    def y_size(self):
        return SPARSE_Y_SIZE
