# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""


class H5FileError(Exception):
    pass


class H5FileAppendError(H5FileError):
    pass
