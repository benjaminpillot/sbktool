import numpy as np
import pandas as pd

from abc import abstractmethod


def diff(array):
    return np.hstack((0, array[1:] - array[:-1]))


class OvenModel:
    """ Class used to implement oven behavior model

    """

    def __init__(self):
        pass

    @abstractmethod
    def number_of_batches(self, *args, **kwargs):
        pass


class SolarThresholdModel(OvenModel):

    def __init__(self, dni_threshold, preheat_time, baking_time):
        """

        Parameters
        ----------
        dni_threshold: float or int
        preheat_time: float or int
            constant preheat time (in hours)
        baking_time: float or int
            constant baking time (in hours)
        """
        super().__init__()
        self.dni_threshold = dni_threshold
        self.preheat_time = preheat_time
        self.baking_time = baking_time

    def number_of_batches(self, dni):
        """ Retrieve the nb of bread batches produced by the oven

        Parameters
        ----------
        dni: numpy.array

        Returns
        -------

        """
        # Nb of continuous operating hours
        # hourly_dni = dni.resample('H').mean()
        operating_hours = (dni >= self.dni_threshold).astype(int)
        in_out = np.arange(operating_hours.size) * np.abs(diff(operating_hours))
        cumul_over_threshold = diff(in_out[in_out != 0])
        cumul_over_threshold[::2] = 0

        # cumul_hours = np.zeros(in_out.shape)
        # cumul_hours[in_out != 0] = cumul_over_threshold
        # cumul_hours = pd.Series(index=self.hourly_dni.index, dtype=np.int16)
        # cumul_hours.values[in_out != 0] = cumul_over_threshold

        # Number of batches
        # nb_batches = pd.Series(index=dni.index, dtype=np.int16)
        nb_batches = np.zeros(dni.size)
        nb_batches[in_out != 0] = (cumul_over_threshold - self.preheat_time) / self.baking_time
        nb_batches[nb_batches < 0] = 0

        return nb_batches
