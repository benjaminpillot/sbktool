from abc import abstractmethod


class Bakery:

    def __init__(self, oven):
        """

        Parameters
        ----------
        oven: sbktool.bakery.Oven
        """
        self.oven = oven


class Oven:

    batch_size = None

    def __init__(self, oven_model):
        """ Implement the bakery's oven characteristics

        Parameters
        ----------
        oven_model: OvenModel
        """
        self.model = oven_model

    @abstractmethod
    def batches(self, *args, **kwargs):
        pass


class SolarOven(Oven):

    def __init__(self, oven_model):
        """ Implement solar oven features

        Parameters
        ----------
        oven_model
        """
        super().__init__(oven_model)

    def batches(self, dni):
        """ Compute nb of batches from DNI

        Returns
        -------
        pandas.Series

        """
        return self.model.number_of_batches(dni)
