from abc import abstractmethod
from functools import partial

import multiprocessing as mp

import numpy as np
import pandas as pd

from sbk.analysis.processing import _get_bakable_day, _get_batches, _get_dni, _get_shadow, \
    _compute_raster, _compute_descriptor
from sbk.iotools import H5File
from sbk.utils import lazyproperty

H5_BAKABLE_DAY_DS_NAME = "bakable_day"
H5_BATCHES_DS_NAME = "nb_batches"
H5_DNI_DS_NAME = "dni"
H5_SHADOW_DS_NAME = "shadow"


class Descriptor:
    """ Mapper class definition
        Provide solar-bread based raster mapping
    """

    dtype = None
    h5_ds_name = None

    replace_dni_no_data_by_0 = True

    _freq = "D"  # Daily frequence

    def __init__(self, oven, solar_db, h5_path):
        """

        Parameters
        ----------
        oven: sbktool.bakery.Oven
        solar_db: sbktool.solar_db.base.RegularSolarDB or sbktool.solar_db.base.SparseSolarDB
        h5_path
        """
        self.oven = oven
        self.solar_db = solar_db

        try:
            h5 = H5File(h5_path)
        except FileNotFoundError:
            raise TypeError(f"'{h5_path}' is not a valid path name")
        else:
            self._h5 = h5_path
            h5.close()

    def compute_raster(self, period=None, start=None, end=None, no_data=-999,
                       nb_processes=mp.cpu_count(), chunk_size=1000, *args, **kwargs):
        """

        Parameters
        ----------
        period: str
            {'D', 'M', 'Y', etc.}
        start
        end
        no_data
        nb_processes
        chunk_size
        args
        kwargs

        Returns
        -------

        """
        if start is None:
            start = self.time[0]

        if end is None:
            end = self.time[-1]

        return _compute_raster(self,
                               start,
                               end,
                               period,
                               no_data,
                               nb_processes,
                               chunk_size)

    def run(self, nb_processes=mp.cpu_count(),
            chunk_size=1000, *args, **kwargs):
        """

        Parameters
        ----------
        nb_processes
        chunk_size
        args
        kwargs

        Returns
        -------

        """

        return _compute_descriptor(self,
                                   nb_processes,
                                   chunk_size)

    @property
    def bounds(self):
        return self.solar_db.bounds

    @property
    def crs(self):
        return self.solar_db.crs

    @property
    @abstractmethod
    def fhandle(self):
        pass

    @property
    def h5(self):
        return self._h5

    @property
    def h5_dni_ds_name(self):
        return self.solar_db.h5_dni_ds_name

    @property
    def region(self):
        return self.solar_db.region

    @lazyproperty
    def time(self):
        return pd.date_range(self.solar_db.time[0],
                             self.solar_db.time[-1],
                             freq=self._freq)

    @property
    def window_step(self):
        return int(1 / self.solar_db.time_resolution)

    @property
    def window_size(self):
        return self.window_step + 1

    @property
    def x_size(self):
        return self.solar_db.x_size

    @property
    def y_size(self):
        return self.solar_db.y_size


class Batches(Descriptor):

    dtype = np.int8
    h5_ds_name = H5_BATCHES_DS_NAME

    # _h5_bakable_day_ds_name = H5_BAKABLE_DAY_DS_NAME

    @property
    def fhandle(self):
        return partial(_get_batches,
                       fhandle=self.oven.batches,
                       h5_from_name=self.solar_db.h5,
                       h5_dni_ds_name=self.h5_dni_ds_name,
                       scale_factor=self.solar_db._solar_radiation_scale_factor,
                       window_size=self.window_size,
                       window_step=self.window_step,
                       no_data=self.solar_db.no_data,
                       replace_no_data_by_0=self.replace_dni_no_data_by_0)


class Bakable(Descriptor):

    dtype = np.int8
    h5_ds_name = H5_BAKABLE_DAY_DS_NAME

    def __init__(self, batches, h5_path):

        super().__init__(batches.oven,
                         batches.solar_db,
                         h5_path)

        self.batches = batches

    @property
    def fhandle(self):
        return partial(_get_bakable_day,
                       h5_from_name=self.batches.h5,
                       h5_batches_ds_name=self.batches.h5_ds_name)


class DNI(Descriptor):

    dtype = np.int16
    h5_ds_name = H5_DNI_DS_NAME

    @property
    def fhandle(self):
        return partial(_get_dni,
                       h5_from_name=self.solar_db.h5,
                       h5_dni_ds_name=self.h5_dni_ds_name,
                       scale_factor=self.solar_db._solar_radiation_scale_factor,
                       window_size=self.window_size,
                       window_step=self.window_step,
                       no_data=self.solar_db.no_data,
                       replace_no_data_by_0=self.replace_dni_no_data_by_0)


class Shadow(Descriptor):

    dtype = np.int16
    h5_ds_name = H5_SHADOW_DS_NAME

    @property
    def fhandle(self):
        return partial(_get_shadow,
                       h5_from_name=self.solar_db.h5,
                       h5_horizon_vs_sun_ds_name=self.solar_db.h5_horizon_vs_sun_ds_name,
                       nb_time_steps_per_day=int(24/self.solar_db.time_resolution))
