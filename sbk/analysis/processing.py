import multiprocessing as mp
from functools import partial

import bottleneck as bn
import h5py
import numpy as np
import pandas as pd
from pyrasta.raster import Raster
from pyrasta.utils import split_into_chunks
from tqdm import tqdm

from sbk.iotools import H5File


def _get_bakable_day(row, h5_from_name, h5_batches_ds_name):

    with h5py.File(h5_from_name, 'r') as h5_from:
        bakable = (h5_from[h5_batches_ds_name][row, :] > 0).astype(int)

    return bakable


def _get_batches(row, fhandle, h5_from_name, h5_dni_ds_name,
                 scale_factor, window_size, window_step,
                 no_data, replace_no_data_by_0):

    with h5py.File(h5_from_name, 'r') as h5_from:
        dni = h5_from[h5_dni_ds_name][row, :] / scale_factor

    if replace_no_data_by_0:
        dni[dni == no_data] = 0
    dni = bn.move_mean(dni, window_size)[::window_step]
    nb_batches = fhandle(dni)
    nb_batches = np.sum(nb_batches.reshape(24, -1), axis=0)

    return nb_batches


def _get_dni(row, h5_from_name, h5_dni_ds_name, scale_factor,
             window_size, window_step, no_data, replace_no_data_by_0):

    with h5py.File(h5_from_name, 'r') as h5_from:
        dni = h5_from[h5_dni_ds_name][row, :] / scale_factor

    if replace_no_data_by_0:
        dni[dni == no_data] = 0
    dni = bn.move_mean(dni, window_size)[::window_step]

    return np.sum(dni.reshape(24, -1), axis=0)


def _get_shadow(row, h5_from_name, h5_horizon_vs_sun_ds_name, nb_time_steps_per_day):

    with h5py.File(h5_from_name, 'r') as h5_from:
        horizon_vs_sun = h5_from[h5_horizon_vs_sun_ds_name][row, :]

    return np.sum(horizon_vs_sun.reshape(nb_time_steps_per_day, -1), axis=0)


def _resample_descriptor(row, h5_path, h5_ds_name, time, start, end, period):

    with h5py.File(h5_path, 'r') as h5:
        value = pd.Series(data=h5[h5_ds_name][row, :], index=time)

    value = value.loc[start:end]

    if period:
        return value.resample(period).sum().mean()
    else:
        return value.sum()


def _compute_raster(descriptor, start, end, period, no_data, nb_processes, chunk_size):
    """

    Parameters
    ----------
    descriptor
    start
    end
    period
    no_data
    nb_processes
    chunk_size

    Returns
    -------

    """
    with mp.Pool(processes=nb_processes) as pool:
        values = list(tqdm(pool.imap(partial(_resample_descriptor,
                                             h5_path=descriptor.h5,
                                             h5_ds_name=descriptor.h5_ds_name,
                                             time=descriptor.time,
                                             start=start,
                                             end=end,
                                             period=period),
                                     range(descriptor.x_size * descriptor.y_size),
                                     chunksize=chunk_size),
                           total=descriptor.x_size * descriptor.y_size, unit_scale=True,
                           desc="Resampling descriptor to raster"))

    array = np.reshape(values, (descriptor.y_size, descriptor.x_size))
    raster = Raster.from_array(array, descriptor.crs, descriptor.bounds, no_data=no_data)

    if descriptor.region is not None:
        raster = raster.clip(mask=descriptor.region, no_data=no_data)

    return raster


def _compute_descriptor(descriptor, nb_processes, chunk_size, *args, **kwargs):
    """

    Parameters
    ----------
    descriptor
    nb_processes
    chunk_size
    args
    kwargs

    Returns
    -------

    """
    generator = range(descriptor.x_size * descriptor.y_size)

    with H5File(descriptor.h5) as h5_to:
        nb_row, nb_col = descriptor.x_size * descriptor.y_size, descriptor.time.size
        h5_to.reset_dataset(descriptor.h5_ds_name,
                            (nb_row, nb_col),
                            dtype=descriptor.dtype)

        # Processing
        pg = tqdm(total=(descriptor.x_size * descriptor.y_size) // chunk_size + 1,
                  desc="Compute descriptor", unit_scale=True)

        for gen in split_into_chunks(generator, chunk_size):

            with mp.Pool(processes=nb_processes) as pool:

                result = np.asarray(pool.map(descriptor.fhandle,
                                             gen,
                                             chunksize=None))

            pg.update(1)
            h5_to.append(descriptor.h5_ds_name, result)

        pg.close()

    return 0
