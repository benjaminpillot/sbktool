import numpy as np
import scipy.stats

from sbk.utils import lazyproperty


class GeoIndicator:
    """ GeoIndicator class definition
        Suitability indicators for socio-technic analysis
        of a solar bread economy and society
    """

    def __init__(self, descriptor, population_grid, population_attr_name="POPULATION"):
        """

        Parameters
        ----------
        descriptor: pyrasta.raster.Raster
            Bread mapping descriptor
        population_grid: geopandas.Dataframe
            Regular grid with counted population
        """
        self.descriptor = descriptor.to_crs(population_grid.crs)
        self.grid = population_grid
        self.pop_name = population_attr_name
        self.nb_inhabitants = self.grid[self.pop_name].sum()
        self.pop_density = self.grid[self.pop_name] / self.grid.area


class SolarBreadSuitability(GeoIndicator):

    def __init__(self, descriptor, population_grid,
                 population_attr_name="POPULATION",
                 bread_consumer_rate=0.6, bakery_rate=1/1000, cost_rate=8,
                 consumption_mean=60, consumption_std=20,
                 nominal_production_mean=15000, nominal_production_std=3000,
                 distance_min=0, distance_std=0.6):
        """ Initialize BreadProduction class instance

        Parameters
        ----------
        descriptor
        population_grid
        population_attr_name
        bread_consumer_rate
        bakery_rate
        cost_rate
        consumption_mean
        consumption_std
        nominal_production_mean: int or float
            Nominal (viable) bread production
            distribution mean (in kg)
        nominal_production_std: int or float
            Nominal (viable) bread production
             distribution std (in kg)
        distance_min: int or float
            in km
        distance_std: int or float
            in km
        """
        super().__init__(descriptor, population_grid, population_attr_name)

        self.consumption_mean, self.consumption_std = consumption_mean, consumption_std
        self.nominal_production_mean, self.nominal_production_std = nominal_production_mean, nominal_production_std
        self.distance_min, self.distance_std = distance_min, distance_std
        self.cost_rate = cost_rate
        self.nb_bakeries = bakery_rate * self.nb_inhabitants
        self.nb_bread_consumers = bread_consumer_rate * self.nb_inhabitants

    def economic_return(self, distance, is_k_euros=True):
        """ Calculate economic return with respect to distance

        Parameters
        ----------
        distance
        is_k_euros

        Returns
        -------

        """

        def surface(dist):
            return np.pi * (1000 * dist) ** 2  # Surface in m2

        try:
            distances = list(distance)
        except TypeError:
            distances = [distance]

        result = {f"RETURN_{d}": None for d in distances}

        # See Pillot et al. for the economic return + probability of consumption formula
        numerator = self.bread_production * self.nb_bakeries * self.nb_inhabitants
        for d in distances:
            denominator = min(self.grid.area.sum() / surface(d), self.nb_bakeries) * \
                          self.pop_density * surface(d) * self.nb_bread_consumers
            probability = 1 - self.bread_consumption_per_capita.cdf(
                numerator / (denominator * self.consumer_distribution.cdf(d)))
            result[f"RETURN_{d}"] = self.bread_production * self.cost_rate * probability / max(1, is_k_euros * 1e3)

        return result

    def solar_bread_accessibility(self,
                                  max_access_distance=0.4,
                                  probability_range=0.95):
        """ Compute SBA

        Parameters
        ----------
        max_access_distance
        probability_range

        Returns
        -------
        SBA in kg/km2

        """
        access_surface_area = np.pi * max_access_distance**2
        p_access = \
            self.nominal_bread_production.cdf(access_surface_area * self.nb_bread_consumers *
                                              self.pop_density *
                                              self.bread_consumption_per_capita.ppf(1 - probability_range) /
                                              self.nb_inhabitants)

        return p_access
        # return access_surface_area / \
        #     np.maximum(access_surface_area,
        #                self._surface_of_consumption(probability_range,
        #                                             max_bread_production))

    def solar_bread_viability(self,
                              max_access_distance=0.4,
                              probability_range=0.95):
        """

        Parameters
        ----------
        max_access_distance: float or int
            maximum radius distance (in km)
            for bread consumers around bakery
        probability_range

        Returns
        -------

        """
        p_prod = self.nominal_bread_production.cdf(self.bread_production)

        return p_prod * self.solar_bread_accessibility(max_access_distance,
                                                       probability_range)

    @property
    def bread_consumption_per_capita(self):
        return scipy.stats.norm(self.consumption_mean,
                                self.consumption_std)

    @property
    def consumer_distribution(self):
        return scipy.stats.halfnorm(self.distance_min,
                                    self.distance_std)

    @lazyproperty
    def bread_production(self):
        return np.asarray(self.descriptor.zonal_stats(self.grid,
                                                      stats=["mean"],
                                                      show_progressbar=False)["mean"])

    @property
    def nominal_bread_production(self):
        return scipy.stats.norm(self.nominal_production_mean,
                                self.nominal_production_std)
