import geopandas as gpd
from gistools.layer import PolygonLayer
from matplotlib import pyplot as plt

file = "/home/benjamin/Documents/PRO/PRODUITS/BD_TOPO_IGN/BDTOPO_3" \
       "-3_TOUSTHEMES_SHP_LAMB93_D076_2023-03-15/BDTOPO/1_DONNEES_LIVRAISON_2023-03-00212/BDT_3" \
       "-3_SHP_LAMB93_D076-ED2023-03-15/ADMINISTRATIF/DEPARTEMENT.shp"
deps = gpd.GeoDataFrame.from_file(file)
dep = PolygonLayer.from_gpd(deps[deps["NOM"] == "Seine-Maritime"])

# Create grid
grid_500 = dep.split(surface_threshold=1e6,
                     method="hexana",
                     no_multipart=True,
                     show_progressbar=False)

grid_500.plot()
plt.show()


