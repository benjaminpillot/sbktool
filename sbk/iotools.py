import numpy as np
import os
import re
import tempfile

import h5py

from sbk.exceptions import H5FileAppendError
from sbk.utils import get_slice_along_axis


def find_file(file, directory=os.getcwd(), sort=True):
    """ Find file(s) in directory

    Description
    -----------

    Parameters
    ----------
    file: str
        file name/extension
    directory: str
        relative/absolute path to directory
    sort: bool
        sort resulting list

    Returns
    -------
    list of file paths: list

    :Example:
        * Look for file extension in current directory
        >>> find_file(".py")
        ["/path/to/file.py", "/path/to/file2.py", ...]

        * Look for file name(s) in specific directory
        >>> find_file("filename", "/path/to/valid/directory")
        ["path/to/this-is-a-filename.txt", "path/to/filename.ods", ...]

        * Look for multiple file names/extension in current directory
        >>> list(set([item for item in find_file(...) for ... in ["filename", ".py"]]))

    """

    result = []

    if file[0] == ".":  # pattern: file extension
        pattern = r'[^\\/:*?"<>|\r\n]+' + re.escape(file) + '$'
    else:  # pattern: file name (special regex characters are regarded as literal)
        pattern = re.escape(file)

    if os.path.isdir(directory):
        for root, dirs, files in os.walk(directory):
            for name in files:
                if re.search(pattern, name) is not None:
                    result.append(os.path.join(root, name))
    else:
        raise ValueError("'{}' is not a valid directory path".format(directory))

    if sort:
        result = sorted(result)

    return result


class H5File:

    def __init__(self, path):
        self.h5 = h5py.File(path, 'a')
        self.path = self.h5.filename
        # Personal note: never put the following line in the class
        # definition (i.e. defined as a class attribute) !! As the
        # dictionary is a mutable object, a modification in any
        # instance will override in all the other instances !!
        self.row_caret_pos = {}

    def __del__(self):
        self.close()

    def __getitem__(self, item):
        if self.has_dataset(item):
            return self.h5[item]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def append(self, dataset_name, data, axis=0):
        """ Append data to dataset (by row)

        Parameters
        ----------
        dataset_name: str
        data
        axis

        Returns
        -------

        """
        if self.has_dataset(dataset_name):
            if dataset_name not in self.row_caret_pos.keys():
                self.row_caret_pos[dataset_name] = 0
            start = self.row_caret_pos[dataset_name]
            self.row_caret_pos[dataset_name] += data.shape[axis]
            idx = get_slice_along_axis(data.ndim, axis,
                                       slice(start, self.row_caret_pos[dataset_name]))
            try:
                self.h5[dataset_name][idx] = data
            except IndexError as e:
                raise H5FileAppendError(e)
            # self.h5[dataset_name][start:self.row_caret_pos[dataset_name], :] = data

    def close(self):
        self.h5.close()

    def copy(self, h5, dataset_name):
        """ Copy H5File dataset to current file

        Parameters
        ----------
        h5: H5File
        dataset_name: str

        Returns
        -------

        """
        if h5.has_dataset(dataset_name) and not self.has_dataset(dataset_name):
            self.h5.create_dataset(dataset_name,
                                   data=h5[dataset_name],
                                   shape=h5[dataset_name].shape,
                                   dtype=h5[dataset_name].dtype)

    def create_dataset(self, name, shape, dtype=np.float32):
        """ Create dataset in H5 file

        Parameters
        ----------
        name
        shape
        dtype

        Returns
        -------

        """
        if not self.has_dataset(name):
            self.h5.create_dataset(name, shape=shape, dtype=dtype)

    def has_dataset(self, name):
        """ Test if h5 file has dataset

        Parameters
        ----------
        name: str

        Returns
        -------
        """
        return name in self.dataset_names

    def read(self, dataset_name, chunk_size, axis=0):
        """ Sequentially read dataset by row with respect to batch size

        Last batch shall have size <= batch_size

        Parameters
        ----------
        dataset_name: str
        chunk_size: int
        axis: int

        Returns
        -------
        """
        nb_iter = self.h5[dataset_name].shape[axis] // chunk_size + \
            min(1, self.h5[dataset_name].shape[axis] % chunk_size)
        if self.has_dataset(dataset_name):
            for i in range(0, nb_iter):
                yield self.h5[dataset_name][get_slice_along_axis(self.h5[dataset_name].ndim, axis,
                                                                 slice(i * chunk_size,
                                                                       (i+1) * chunk_size))]
                # yield self.h5[dataset_name][i * chunk_size:(i + 1) * chunk_size, :]

    def read_at(self, dataset_name, indices, axis=0):
        """ Read data in dataset at given indices

        Parameters
        ----------
        dataset_name: str
            Name of the dataset
        indices: numpy.ndarray
            indices for which must be returned data in dataset
        axis: int
            Axis for which indices are defined

        Returns
        -------

        """
        if self.has_dataset(dataset_name):
            return self.h5[dataset_name][get_slice_along_axis(self.h5[dataset_name].ndim,
                                                              axis, indices)]

    def read_at_random(self, dataset_name, batch_size, axis=0, max_iter=1000):
        """ Read dataset at random row by row with respect to batch size

        Parameters
        ----------
        dataset_name: str
            Dataset name
        batch_size: int
            Size of each batch to read from dataset
        axis: int
            Axis along which data must be read row by row
        max_iter: int
            Maximum number of iterations for batch process

        Returns
        -------
        """
        rng = np.random.default_rng()
        if self.has_dataset(dataset_name):
            for _ in range(max_iter):
                items = sorted(rng.choice(self.h5[dataset_name].shape[axis],
                                          size=batch_size,
                                          replace=False,
                                          shuffle=False))
                yield self.h5[dataset_name][get_slice_along_axis(self.h5[dataset_name].ndim,
                                                                 axis, items)]
                # yield self.h5[dataset_name][items, :]

    def remove_dataset(self, dataset_name):
        """ Remove dataset within h5 file

        Parameters
        ----------
        dataset_name:

        Returns
        -------
        """
        if self.has_dataset(dataset_name):
            self.reset_caret(dataset_name)
            del self.h5[dataset_name]

    def reset_caret(self, dataset_name):
        try:
            self.row_caret_pos.pop(dataset_name)
        except KeyError:
            pass

    def reset_dataset(self, dataset_name, shape, dtype=np.float32):
        """ Reset dataset

        Parameters
        ----------
        dataset_name: str
            Dataset name
        shape: tuple
            Shape of the dataset table
        dtype:

        Returns
        -------
        """
        self.remove_dataset(dataset_name)
        self.create_dataset(dataset_name, shape, dtype)

    @property
    def dataset_names(self):
        return list(self.h5.keys())

    @property
    def attributes(self):
        return self.h5.attrs

    @property
    def attribute_names(self):
        return list(self.h5.attrs.keys())


class H5TempFile(H5File):

    def __init__(self):

        super().__init__(tempfile.mkstemp(suffix='.h5')[1])

    def __del__(self):
        super().__del__()
        try:
            os.remove(self.path)
        except FileNotFoundError:
            pass
